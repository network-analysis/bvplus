BV+
=====

The implementation of the algorithm proposed in: P. Liakos, K. Papakonstantinopoulou, M. Sioutis. Pushing the Envelope in Graph Compression. In CIKM, 2014.

The graph web-Stanford.llp.txt from [SNAP](https://snap.stanford.edu/data/web-Stanford.html) is included for testing purposes. The graph has been reordered with the use of the algorithn proposed in  P. Boldi, M. Rosa, M. Santini, and S. Vigna. Layered Label Propagation: A MultiResolution Coordinate-Free Ordering for Compressing Social Networks. In WWW, 2011.

Visit our [group](http://hive.di.uoa.gr/network-analysis) for more details.

How to Use
-----


```shell
$ mvn clean install
$ java -cp target/BVPlus-1.0.0-jar-with-dependencies.jar gr.di.uoa.a8.bvplus.BVPlusGraph src/main/resources/web-Stanford.llp.txt
```